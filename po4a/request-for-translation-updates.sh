#! /bin/sh

# request-for-translation-updates.sh - request for translation updates for the
#  debian-history package
# by Joost van Baal-Ilić, 2024
# This file is distributed under the same license as the debian-history
# package.

# This script needs to be called from within a (possibly read-only) git
# checkout of the debian-history package.  It sends out email using mutt(1), so
# the host needs to be configured to send out email.

# NB TODO FIXME WARNING this script is as of yet ( уто,  6. авг 2024. )
# only usable by the author.  It has been used only once.

# See https://www.debian.org/international/l10n/po4a/pot ,
# https://i18n.debian.org/ and https://l10n.debian.org/
# for debian and po

# call as $ rftu_DEBUG=true rftu_DRYRUN=true request-for-translation-updates.sh
# to run in debug mode
debug=${rftu_DEBUG:-false}
dryrun=${rftu_DRYRUN:-false}

cat <<EOT | while read l lang email name
de german     hwansing@mailbox.org Holger Wansing
es spanish    larjona@debian.org   Laura Arjona Reina
fr french     dlist@bluewin.ch     Steve Petruzzello
it italian    beatricet@libero.it  Beatrice Torracca
ja japanese   yy.y.ja.jp@gmail.com YOSHINO Yoshihito
ko korean     sebuls@gmail.com     Sangdo Jun
lt lithuanian kebil@kaunas.init.lt Kęstutis Biliūnas
pt portuguese a_monteiro@gmx.com   Américo Monteiro
ru russian    l.lamberov@gmail.com Lev Lamberov
EOT
do
    # FIXME debian-japanese@lists.debian.org is the list for japanese l10n

    to="$email"
    cc="debian-l10n-$lang@lists.debian.org"
    # FIXME also cc debian-publicity?
    # FIXME debugging hardcoded to author's local username
    $debug && { to="joostvb"; cc="joostvb"; }

    # FIXME http://mdcc.cx hardcoded in below body text
    body=$( cat <<EOT
Hi $name,

I am writing to you since you are listed as "Last-Translator" for the $lang
translation for the document "A Brief History of Debian" as shipped with the
debian-history package. The English template has been changed (see
http://mdcc.cx/tmp/debian-history/debian-history_13.0/usr/share/doc/debian-history/docs/
for fully typesetted examples from a local build of debian-history 13.0 as
currently in sid), and now some messages are marked "fuzzy" in your translation
or are missing.

I would be grateful if you could take the time and update it.  Please send the
updated file to me, or submit it as a wishlist bug against debian-history, or
via https://salsa.debian.org/publicity-team/debian-history (according to your
personal preferences).

For your convenience I've attached po4a/po/$l.po as currently in the
debian-history git repository.

Bye,

Joost
EOT
      )

    $debug && message="about to call"
    $dryrun && message="would have called"

    $debug && echo >&2 $message mutt -c "$cc" -s "Request for update of $lang translation of debian-history" -a po/$l.po -- "$name <$to>"
    $debug && echo >&2 using body:
    $debug && cat <<EOT
$body
EOT
    $dryrun || cat <<EOT | mutt -c "$cc" -s "Request for update of $lang translation of debian-history" -a po/$l.po -- "$name <$to>"
$body
EOT

    # multiple Cc's:
    # mutt -c "foo@bar.com, duh@example.com"
    # use '-b "foo@bar.com"' for Bcc
done

# idea: use ~/postponed for debugging? tl;dr: didn't get to work that:
#
# mutt -F /dev/null -e "set postpone=yes" -a attach.txt -- joostvb < foo.msg
#
# % cat <<EOT | mutt -F /dev/null -e "set postpone=yes" -a /etc/motd -- joostvb
# pipe heredoc> From: <joostvb>
# pipe heredoc> To: <joostvb>
# pipe heredoc> Subject: test, please ignore
# pipe heredoc>
# pipe heredoc> foo
# pipe heredoc> EOT
#
# helaas pindakaas: ends up in my inbox via /usr/sbin/sendmail, not as-is in
#  ~/postponed
#
# use Mail::Mailer(3pm) ?
# mailer = Mail::Mailer->new("testfile", @args);
# MIME::Tools(3pm) looks promissing
